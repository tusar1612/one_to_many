<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/quarters', function () {
   
    $quarter=\App\Quarter::find(2)->course;

    return $quarter;
});


Route::get('/courses', function () {

    $course=\App\Course::find(4)->quarter;

    return $course;
});